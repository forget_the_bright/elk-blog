;(function ($, window, document, undefined) {
    var Drawer = function (ele, opt) {
        this.$element = ele,
            this.defaults = {
                'color': 'red',
            },
            this.options = $.extend({}, this.defaults, opt)
    }
    Drawer.prototype = {
        init: function () {
            this.renderTpl()
            this.getDom()
            this.bind()
        },
        renderTpl: function () {
            $('body').append(this.options.tpl.html())
        },
        getDom: function () {
            var _dom = null
            this.dom = {}

            if (this.options.dom) {
                _dom = this.options.dom
                for (var p in _dom) {
                    this.dom[p] = $(_dom[p])
                }
            }
        },
        bind: function () {
            var self = this,
                container = self.dom.container,
                close = self.dom.close,
                content = self.dom.main.find('.app-drawer-content'),
                mask = container.find('.app-drawer-mask'),
                openCls = 'app-drawer-open'

            self.dom.close.on('click', function(){
                var scope = $(this)

                if (container.hasClass(openCls)) {
                    container.removeClass(openCls)
                    content.css({ width: 0 })
                    close.find('i').addClass('icon-setting').removeClass('icon-close')
                } else {
                    container.addClass(openCls)
                    content.css({ width: 300 })
                    close.find('i').removeClass('icon-setting').addClass('icon-close')
                }
            })

            mask.on('click', function(){
                container.removeClass(openCls)
                content.css({ width: 0 })
            })
        },
        render: function () {

        }
    }

    $.settingDrawer = function (options) {
        var drawer = new Drawer(this, options)
        return drawer.init()
    }
})(jQuery, window, document)
