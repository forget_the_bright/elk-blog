package main

import (
	// "gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/app/model"
	"gitee.com/jikey/elk-blog/routers"
	"gitee.com/jikey/elk-blog/setting"
	"github.com/sirupsen/logrus"
	"time"
)

// 初始化应用
func init() {
	// 设置时区
	timelocal, _ := time.LoadLocation("Asia/Shanghai")
	time.Local = timelocal

	router := routers.RouterApp()
	_ = router.Run(setting.Config.Server.Port)

	// 设置日志级别
	logrus.SetLevel(logrus.DebugLevel)
	logrus.Info("App start")
}

func main() {
	model.CloseDB()
}
