package model

type System struct {
	BaseModel
	Domain       string `gorm:"column:domain" json:"domain" form:"domain"`
	SiteName     string `gorm:"column:site_name" json:"site_name" form:"site_name"`
	Description  string `gorm:"column:description" json:"description" form:"description"`
	Keyword      string `gorm:"column:keyword" json:"keyword" form:"keyword"`
	Email        string `gorm:"column:email" json:"email" form:"email"`
	Contact      string `gorm:"column:contact" json:"contact" form:"contact"`
	Company      string `gorm:"column:company" json:"company" form:"company"`
	Record       string `gorm:"column:record" json:"record" form:"record"`
	Phone        string `gorm:"column:phone" json:"phone" form:"phone"`
	Icp          string `gorm:"column:icp" json:"icp" form:"icp"`
	Address      string `gorm:"column:address" json:"address" form:"address"`
	Tpl          string `gorm:"column:tpl" json:"tpl" form:"tpl"`
	AllowComment bool   `gorm:"column:allow_comment" json:"allow_comment" form:"allow_comment"`
	ShowBanner   bool   `gorm:"column:show_banner" json:"show_banner" form:"show_banner"`
}
