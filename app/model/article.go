package model

import (
	"errors"
)

type Article struct {
	BaseModel
	Category    Category `gorm:"foreignkey:Catid"`
	Title       string   `gorm:"column:title" json:"title" form:"title"`
	Content     string   `gorm:"column:content" json:"content" form:"content"`
	Md          string   `gorm:"column:md" json:"md" form:"md"`
	Description string   `gorm:"column:description" json:"description" form:"description"`
	Catid       int      `gorm:"column:catid" json:"catid" form:"catid"`
	// CategoryId  int      `gorm:"column:category_id" json:"category_id" form:"category_id"`
	Status    int    `gorm:"column:status" json:"status" form:"status"`
	Likes     int    `gorm:"column:likes" json:"likes" form:"likes"`
	Views     int    `gorm:"column:views" json:"views" form:"views"`
	Avatar    string `gorm:"column:avatar;type:varchar(200)" json:"avatar"`
	Username  string `gorm:"column:username" json:"username" form:"username"`
	Author    string `gorm:"column:author" json:"author" form:"author"`
	Total     string `gorm:"-"`
	SourceUrl string `gorm:"column:source_url;type:varchar(200)" json:"source_url"`
	ColTitle  string `gorm:"column:colTitle" json:"colTitle"`
}

func (b *Article) IsValid() (err error) {
	if b.Title == "" {
		err = errors.New("标题不能为空")
	}
	return
}
