package model

type Link struct {
	BaseModel
	Linktype  int    `gorm:"column:linktype;type:tinyint(1)" json:"linktype"`
	Title     string `gorm:"column:title;type:varchar(50)" json:"title"`
	Url       string `gorm:"column:url;type:varchar(255)" json:"url"`
	Logo      string `gorm:"column:logo;type:varchar(255)" json:"logo"`
	Status    int    `gorm:"column:status;type:tinyint(1);default:1" json:"status"`
	Ord       int    `gorm:"column:ord;type:tinyint(5);default:0" json:"ord"`
	Introduce string `gorm:"column:introduce;type:text" json:"introduce"`
	Contact   string `gorm:"column:contact;type:varchar(30)" json:"contact"`
}
