package home

import (
    "fmt"
    "gitee.com/jikey/elk-blog/app/model"
    "gitee.com/jikey/elk-blog/app/service"
    "gitee.com/jikey/elk-blog/pkg/response"
    "gitee.com/jikey/elk-blog/pkg/utils"
    "gitee.com/jikey/elk-blog/setting"
    "github.com/gin-gonic/gin"
    "github.com/sirupsen/logrus"
    "html/template"
    "strconv"
    "strings"
)

type Article struct {
    Controller
}

func init() {
    Article := &Article{}
    _ = Article.Init(Article)
    Article.Menu()

    Elk = make(map[interface{}]interface{})
    Elk["Menu"] = Article.Data["Menu"]
    Elk["ArticleCategory"] = Article.Data["ArticleCategory"]
    Elk["ArchiveList"] = Article.Data["ArchiveList"]
}

// Index 首页
func (b *Article) Index(c *gin.Context) {
    var conditions []string
    data := make(map[string]interface{})
    pageNum, _ := strconv.Atoi(c.DefaultQuery("page", "1"))

    username := c.Query("username")
    keywords := c.Query("keywords")
    maps := ""

    conditions = append(conditions, "tk_article.status = 1")

    if len(keywords) > 0 {
        conditions = append(conditions, fmt.Sprintf("title like '%%%s%%'", keywords))
    }

    if len(username) > 0 {
        conditions = append(conditions, fmt.Sprintf("username like '%%%s%%'", username))
    }

    maps = strings.Join(conditions, " AND ")

    // 获取分页数据
    page := utils.NewPagination(c.Request, service.Article.GetArticleTotal(maps), setting.Config.App.PageSize)
    data["page"] = template.HTML(page.Pages())

    data["menu"] = Elk["Menu"]
    data["articleCategory"] = service.Category.GetArticleCategoryList("4")
    data["archiveList"] = service.Article.ArticleArchiveList()
    data["list"] = service.Article.ArticleList(pageNum, setting.Config.App.PageSize, maps)
    data["link"] = service.Link.List(1, 100, "")
    data["site"] = service.System.GetSystem(1)
    data["active"] = "article"

    response.Render(c, "article/index", data)
}

// Detail 详情
func (b *Article) Detail(c *gin.Context) {
    data := make(map[string]interface{})

    id, _ := strconv.Atoi(c.Param("id"))
    pageNum, _ := strconv.Atoi(c.Query("page"))
    maps := fmt.Sprintf("pid = 0 and bid = %d", id)

    detail, _ := service.Article.ArticleDetail(id)
    data["article"] = detail
    data["articleCategory"] = service.Category.GetArticleCategoryList("4")
    data["archiveList"] = service.Article.ArticleArchiveList()

    data["commentList"] = service.Comment.List(pageNum, setting.PageSize, maps)
    data["commentTotal"] = service.Comment.Total(maps)
    data["prev"] = service.Article.GetPrevNextArticle(detail.CreateTime, "prev")
    data["next"] = service.Article.GetPrevNextArticle(detail.CreateTime, "next")

    data["link"] = service.Link.List(1, 100, "")
    data["site"] = service.System.GetSystem(1)
    data["active"] = "article"

    response.Render(c, "article/detail", data)
}

// Archive 归档
func (b *Article) Archive(c *gin.Context) {
    data := make(map[string]interface{})

    pageNum, _ := strconv.Atoi(c.Query("page"))
    total, _ := strconv.Atoi(c.Query("total"))
    id := c.Param("id")

    // 获取分页数据
    page := utils.NewPagination(c.Request, total, setting.PageSize)

    data["page"] = template.HTML(page.Pages())
    data["articleCategory"] = service.Category.GetArticleCategoryList("4")
    data["archiveList"] = service.Article.ArticleArchiveList()
    data["list"] = service.Article.GetArchive(pageNum, setting.PageSize, id)
    data["link"] = service.Link.List(1, 100, "")
    data["site"] = service.System.GetSystem(1)
    data["active"] = "article"

    response.Render(c, "archive/index", data)
}

// Category 分类
func (b *Article) Category(c *gin.Context) {
    data := make(map[string]interface{})
    maps := make(map[string]interface{})

    pageNum, _ := strconv.Atoi(c.Query("page"))

    id := c.Param("id")
    maps["catid"] = id

    // 获取分页数据
    page := utils.NewPagination(c.Request, service.Article.GetArticleTotal(maps), setting.PageSize)

    data["page"] = template.HTML(page.Pages())
    data["articleCategory"] = service.Category.GetArticleCategoryList("4")
    data["archiveList"] = service.Article.ArticleArchiveList()
    data["list"] = service.Article.ArticleList(pageNum, setting.PageSize, maps)
    data["link"] = service.Link.List(1, 100, "")
    data["site"] = service.System.GetSystem(1)
    data["active"] = "article"

    response.Render(c, "category/index", data)
}

// Search 搜索
func (b *Article) Search(c *gin.Context) {
    data := make(map[string]interface{})
    maps := make(map[string]interface{})

    // 获取url 参数信息
    pageNum := 1
    if pageQuery := c.Query("page"); len(pageQuery) > 0 {
        var err error
        pageNum, err = strconv.Atoi(pageQuery)
        if err != nil {
            logrus.Error("分页参数转换有误")
            return
        }
    }

    keywords := c.Query("keywords")
    maps["title"] = keywords

    // 获取分页数据
    page := utils.NewPagination(c.Request, service.Article.GetArticleTotal(maps), setting.PageSize)

    data["page"] = template.HTML(page.Pages())
    data["list"] = service.Article.ArticleList(pageNum, setting.PageSize, maps)
    data["site"] = service.System.GetSystem(1)
    data["active"] = "article"

    response.Render(c, "article/search", data)
}

// Likes 点赞
func (b *Article) Likes(c *gin.Context) {
    id := c.Param("id")

    Article := &model.Article{}

    if err := c.ShouldBind(&Article); err != nil {
        c.JSON(400, gin.H{
            "err": err.Error(),
        })
        return
    }
    fmt.Println("Article", Article.Likes)

    service.Article.UpdateField(id, "likes", Article.Likes)

    response.Success(c, Article.Likes)
}
