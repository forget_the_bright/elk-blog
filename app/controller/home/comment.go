package home

import (
	"gitee.com/jikey/elk-blog/app/model"
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/e"
	"gitee.com/jikey/elk-blog/pkg/response"
	"gitee.com/jikey/elk-blog/setting"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"strconv"
)

type Comment struct {
	Controller
}

// List 列表
func (m *Comment) List(c *gin.Context) {
	data := make(map[string]interface{})

	pageNum, _ := strconv.Atoi(c.Query("page"))

	title := c.Query("content")
	maps := "pid = 0"

	if len(title) > 0 {
		maps += " and content like '%" + title + "%'"
	}

	data["rows"] = service.Comment.List(pageNum, setting.PageSize, maps)
	data["total"] = service.Comment.Total(maps)

	response.Success(c, data)
}

// Add 新增
func (m *Comment) Add(c *gin.Context) {
	comment := &model.Comment{}

	if err := c.ShouldBind(&comment); err != nil {
		c.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}

	err := service.Comment.Create(comment)

	if err != nil {
		logrus.Error("新增失败", err)
		response.Fail(c, e.ErrorInsert)
		return
	}

	response.Success(c, comment)
}

// Destory 删除
func (m *Comment) Destory(c *gin.Context) {
	id := c.PostForm("id")
	service.Comment.Delete(id)

	response.Success(c, id)
}
