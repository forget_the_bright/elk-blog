package home

import (
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/response"
	"github.com/gin-gonic/gin"
)

type About struct {
	Controller
}

func (about *About) Index(c *gin.Context) {
	data := make(map[string]interface{})
	data["site"] = service.System.GetSystem(1)
	data["active"] = "about"
	data["about"], _ = service.About.AboutDetail(1)

	response.Render(c, "about/index", data)
}
