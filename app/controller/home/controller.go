package home

import (
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/setting"
	"github.com/gin-gonic/gin"
	"reflect"
	"strings"
)

type ControllerInterface interface {
	beforeAction(context *gin.Context) error
	afterAction(context *gin.Context) error
}

type Controller struct {
	Data           map[interface{}]interface{}
	controllerName string
	actionName     string
	viewDir        string              // view 默认文件夹（相对路径）
	this           ControllerInterface // 实例
}

var Site = setting.Config.Site

func (ctrl *Controller) Init(this ControllerInterface) error {
	ctrl.this = this
	className := reflect.TypeOf(ctrl.this).String()
	className = className[strings.LastIndex(className, ".")+1:]
	ctrl.actionName = "className"
	ctrl.Data = make(map[interface{}]interface{})
	return nil
}

/*
*
action调用前回调
*/
func (ctrl *Controller) beforeAction(context *gin.Context) error {
	return nil
}

/*
*
action调用后回调
*/
func (ctrl *Controller) afterAction(context *gin.Context) error {
	return nil
}

func (ctrl *Controller) Menu() {
	ctrl.Data["Menu"] = "menu"
	ctrl.Data["ArticleCategory"] = service.Category.GetArticleCategoryList("4")
}
