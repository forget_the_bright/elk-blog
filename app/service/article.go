package service

import (
	"fmt"
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"strings"
)

type article struct {
}

var Article *article

// ArticleList 获取 article 列表
func (b *article) ArticleList(pageNum, pageSize int, filters interface{}) []model.Article {
	var articles []model.Article

	err := db.Mysql.
		Model(&model.Article{}).
		Joins("left join tk_category on tk_category.colId = tk_article.catid").
		Where(filters).
		Offset((pageNum - 1) * pageSize).
		Limit(pageSize).
		Order("ord asc, id desc").
		Select("tk_article.*, tk_category.colTitle").
		Find(&articles).
		Error

	if err != nil {
		logrus.Error("获取 article 列表失败", err)
		return nil
	}

	return articles
}

// GetBy 查询
func (b *article) GetBy(field, value string) (article []model.Article) {
	db.Mysql.Where("? = ?", field, value).First(&article)
	return
}

// GetArchiveList 获取归档列表
func (b *article) GetArchiveList(pageNum int, pageSize int, maps interface{}) (article []model.Article) {
	db.Mysql.Where(maps).Offset((pageNum - 1) * pageSize).Limit(pageSize).Order("ord asc, id desc").Find(&article)
	return
}

// ArticleDetail 获取 article 详情
func (b *article) ArticleDetail(id int) (marticle model.Article, err error) {
	err = db.Mysql.First(&marticle, id).Error
	db.Mysql.Model(&marticle).Where("id = ?", id).UpdateColumn("views", gorm.Expr("views + ?", 1))
	return
}

// GetArticleTotal 获取 article 总记录数
func (b *article) GetArticleTotal(maps interface{}) (count int) {
	db.Mysql.Model(&article{}).Where(maps).Count(&count)
	return
}

// Create 新增
func (b *article) Create(model *model.Article) (err error) {
	err = db.Mysql.Omit("ColTitle").Create(model).Error

	return err
}

// Update 修改
func (b *article) Update(id string, model *model.Article) (err error) {
	err = db.Mysql.Model(&model).Where("id = ? ", id).Omit("ColTitle").Updates(model).Error

	return err
}

// UpdateField 修改某一字段
func (b *article) UpdateField(id string, key string, value interface{}) {
	marticle := &model.Article{}
	db.Mysql.Model(&marticle).Where("id = ?", id).Update(key, value)
}

// Delete 删除
func (b *article) Delete(id string) bool {
	db.Mysql.Where("id = ?", id).Delete(&article{})

	return true
}

// ArticleArchiveList 归档列表
func (b *article) ArticleArchiveList() (archive []model.Archive) {
	sql := "(select count(DATE_FORMAT(create_time, '%Y')) as Count, DATE_FORMAT(create_time, '%Y') as Time,create_time" +
		" as CreateTime from tk_article WHERE YEAR(create_time) < YEAR(NOW()) group by Time order by Time desc) union " +
		"(select count(DATE_FORMAT(create_time, '%Y-%m')) as Count,DATE_FORMAT(create_time, '%Y-%m') as" +
		" Time,create_time as CreateTime from tk_article WHERE YEAR(create_time) = YEAR(NOW()) group by " +
		"Time order by Time desc)"
	db.Mysql.Raw(sql).Scan(&archive)

	return
}

// GetArchive 归档列表
func (b *article) GetArchive(PageNum int, PageSize int, ctime string) (article []model.Article) {
	if find := strings.Contains(ctime, "-"); find {
		fmt.Println("find the character.")
	} else {
		fmt.Println("do not find the character.")
	}
	db.Mysql.Where("create_time like ?", "%"+ctime+"%").Offset((PageNum - 1) * PageSize).Limit(PageSize).Order("ord asc, id desc").Find(&article)
	return
}

// GetPrevNextArticle 获取上一篇下一篇
func (b *article) GetPrevNextArticle(ctime string, dir string) (link string) {
	article := &model.Article{}
	var maps, order string

	if dir == "prev" {
		maps = fmt.Sprintf("create_time > '%s'", ctime)
		order = "asc"
	}

	if dir == "next" {
		maps = fmt.Sprintf("create_time < '%s'", ctime)
		order = "desc"
	}

	db.Mysql.Where(maps).Order("ord asc,create_time " + order).First(&article)

	link = fmt.Sprintf("<a href='/article/%s' class='app-prev-next-link' target='_blank'>%s</a>", cast.ToString(article.Id), article.Title)

	return
}
