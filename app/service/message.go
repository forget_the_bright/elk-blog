package service

import (
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
)

type message struct {
}

var Message *message

func (m *message) List(PageNum int, PageSize int, maps interface{}) (message []*model.Message) {
	db.Mysql.Where(maps).Offset((PageNum - 1) * PageSize).Limit(PageSize).Order("ord asc, id desc").Find(&message)
	return
}

func (m *message) Create(model *model.Message) (err error) {
	err = db.Mysql.Create(model).Error

	return err
}

// Total 获取 blog 总记录数
func (m *message) Total(maps interface{}) (count int) {
	db.Mysql.Model(&message{}).Where(maps).Count(&count)
	return
}

// Delete 删除
func (m *message) Delete(ids interface{}) (rows int64) {
	result := db.Mysql.Delete(&message{}, "id in (?)", ids)

	return result.RowsAffected
}
