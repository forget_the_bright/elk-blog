package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"regexp"
	"strconv"
	"strings"
)

// BcryptHash 使用 bcrypt 对密码进行加密
func BcryptHash(password string) string {
	// GenerateFromPassword 的第二个参数是 cost 值。建议大于 12，数值越大耗费时间越长
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	logrus.Error(err)

	return string(bytes)
}

// BcryptCheck 对比明文密码和数据库的哈希值
func BcryptCheck(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// BcryptIsHashed 判断字符串是否是哈希过的数据
func BcryptIsHashed(str string) bool {
	// bcrypt 加密后的长度等于 60
	return len(str) == 60
}

func RepImages(htmls string) {
	var imgRE = regexp.MustCompile(`<img[^>]+\bsrc=["']([^"']+)["']`)
	imgs := imgRE.FindAllStringSubmatch(htmls, -1)
	out := make([]string, len(imgs))
	myImage := "https://static.juzizhou.net/1111111111111111.jpg"
	resHtml := htmls
	for i := range out {
		if strings.HasPrefix(imgs[i][1], "https://static.juzizhou.net") {
			resHtml = strings.Replace(resHtml, imgs[i][1], myImage, -1)
			out[i] = imgs[i][1]
			fmt.Println(strconv.Itoa(i), out[i])
		}
	}
	fmt.Println(resHtml)
}

func main() {
	a := BcryptHash("admin123$")
	fmt.Print(a)
}
