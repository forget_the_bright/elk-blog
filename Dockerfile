FROM golang:1.17 as golang
# 为我们的镜像设置必要的环境变量
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    GOPROXY=https://goproxy.cn,direct
# 进入工作目录
WORKDIR /blog
ADD . .
# 复制项目中的 go.mod 和 go.sum文件并下载依赖信息
COPY go.mod .
COPY go.sum .
RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags '-w -s' -installsuffix 'public' -o main .

FROM ubuntu:18.04

# 配置项目环境（根据自己情况来）
ENV GO_PROFILE=pro
ENV GIN_MODE=release

# 暴露服务端口
EXPOSE 8080

WORKDIR /app

# 复制打包的 Go 文件到系统用户可执行程序目录下
COPY --from=golang /blog/main .
COPY --from=golang /blog/public ./public
COPY --from=golang /blog/setting ./setting
COPY --from=golang /blog/views ./views

RUN chown -R root /app
RUN chmod +x main

# 容器启动时运行的命令
ENTRYPOINT ["./main"]
#CMD ["/app/ginserver"]